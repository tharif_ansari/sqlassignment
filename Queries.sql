--Update Profit of all the movies by +1000 where producer name contains 'run'

update Movies
set Profit = Profit+1000 
where Movies.ProducerId in
(
select Producers.Id 
from Producers
where Name like('%run%')
);

--List down actors details which have acted in movies where producer name ends with 'n'

select * from Actors 
join MovieActorMapping on Actors.Id=MovieActorMapping.ActorsId
join Movies on MovieActorMapping.MovieId = Movies.Id
join Producers on Movies.ProducerId = Producers.Id 
where Producers.Name Like ('%n');

--Find the avg age of male and female actors that were part of a movie called 'Terminal'  --Should return two rows 

SELECT AVG(DATEDIFF(year, Actors.DOB,   CONVERT (DATE, SYSDATETIME()) )) as Age, Actors.Gender from Actors
join  MovieActorMapping on MovieActorMapping.ActorsId= Actors.Id
join Movies on movies.Id = MovieActorMapping.MovieId
where Movies.Name = 'Terminal' 
group by Actors.gender;

--Find the third oldest female actor


SELECT (DATEDIFF(year, Actors.DOB,   CONVERT (DATE, SYSDATETIME()) )) as Age, Actors.Name from Actors
where Actors.Gender = 'Female'
order by age desc
offset 2 row 
fetch next 1 rows only ; --alternative for LIMIT in postgresql

--List down top 3 profitable movies 

select Movies.Name, Profit from Movies
order by Movies.Profit desc
offset 0 row
fetch next 3 rows only;


--Find duplicate movies having the same name and their count


SELECT   Movies.Name, COUNT(Name) AS duplicate_count
FROM     Movies
GROUP BY Name
HAVING   COUNT(Name) > 1
ORDER BY COUNT(Name) DESC

--List down all the producers and +the movie name(even if they dont have a movie)

select Producers.Name, Movies.Name 
from Movies right join Producers on
Producers.Id=Movies.ProducerId;

--List down the oldest actor and Movie Name for each movie

select Actors.Name, Movies.Name from Actors
join  MovieActorMapping on MovieActorMapping.ActorsId= Actors.Id
join Movies on movies.Id = MovieActorMapping.MovieId
where Actors.DOB = (select min(dob) from Actors);

--Normalise it (If not normalised)

--The table is normalised...
