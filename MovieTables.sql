--https://docs.google.com/document/d/1M4X3HCP6_py4mWlQgfJqjOW3ICQpuhpHrKXt0x3mQrw/edit
CREATE TABLE Producers (Id int identity primary key,Name varchar(20) ,sex varchar(6), DOB date, Bio varchar(100));
CREATE TABLE Actors (Id int identity primary key,Name varchar(20) ,sex varchar(6), DOB date, Bio varchar(100));
CREATE TABLE Movies (Id int identity primary key,Name varchar(100) , YearOfRelease int, Plot varchar(100),ProducerId int references Producers(Id));
CREATE TABLE Genre(Id int identity primary key,Name varchar(20));
CREATE TABLE MovieGenreMapping(Id int identity primary key, MovieId int references Movies(id),GenreId int references Genre(id));
CREATE TABLE MovieActorMapping (Id int identity primary key,MovieId int references Movies(id),ActorsId int references Actors(id));

